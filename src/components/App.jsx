import React, { Component } from 'react'
import Navigation from './navigation';
import Header from './header';
import NewAbout from './NewAbout';
import NewSkill from './NewSkill';
import NewPortofolio from './NewPortofolio';
import NewExperience from './NewExperience';
import NewContact from './NewContact';
import $ from 'jquery';

export class App extends Component {
  state = {
    resumeData : {},
  }
  getResumeData(){
    $.ajax({
      url:'/data.json',
      dataType:'json',
      cache: false,
      success: function(data){
        this.setState({resumeData: data});
      }.bind(this),
      error: function(xhr, status, err){
        console.log(err);
        alert(err);
      }
    });
  }

  componentDidMount(){
    this.getResumeData();
  }

  render() {
    return (
      <div>
        <Navigation />
        <Header data={this.state.resumeData.Header}/>
        <NewAbout data={this.state.resumeData.NewAbout}/>
        <NewExperience  data={this.state.resumeData.NewExperience}/>
        <NewSkill  data={this.state.resumeData.NewSkill}/>
        <NewPortofolio />
        <NewContact  data={this.state.resumeData.NewContact}/>
      </div>
    )
  }
}

export default App
