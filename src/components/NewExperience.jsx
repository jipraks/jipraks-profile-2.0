import React, { Component } from "react";

export class NewExperience extends Component {
  render() {
    return (
      <div id="_experience">
        <div className="container">
          <div className="section-title text-center">
            <h2>Professional Experience</h2>
          </div>
          <div className="row">
            {this.props.data
              ? this.props.data.map((d, i) => (
                <div key={`${d.name}-${i}`} className="col-md-4">
                  <div className="testimonial">
                    <div className="testimonial-image">
                      {" "}
                      <img src={d.img} alt="" />{" "}
                    </div>
                    <div className="testimonial-content">
                      <div className="testimonial-meta">{d.title} at {d.company}</div>
                      <div className="company-experience">{d.text}</div>
                      <div className="testimonial-meta"> - {d.name} </div>
                    </div>
                  </div>
                </div>
              ))
              : "loading"}
          </div>
        </div>
      </div>
    );
  }
}

export default NewExperience;
