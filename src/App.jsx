import React, { Component } from 'react'
import Navigation from './components/navigation';
import Header from './components/header';
import NewAbout from './components/NewAbout';
import NewSkill from './components/NewSkill';
import NewPortofolio from './components/NewPortofolio';
import NewExperience from './components/NewExperience';
import NewContact from './components/NewContact';
import JsonData from './data/data.json';

export class App extends Component {
  state = {
    landingPageData: {},
  }
  getlandingPageData() {
    this.setState({landingPageData : JsonData})
  }

  componentDidMount() {
    this.getlandingPageData();
  }

  render() {
    return (
      <div>
        <Navigation />
        <Header data={this.state.landingPageData.Header} />
        <NewAbout data={this.state.landingPageData.NewAbout} />
        <NewExperience data={this.state.landingPageData.NewExperience} />
        <NewSkill data={this.state.landingPageData.NewSkill} />
        <NewPortofolio data={this.state.landingPageData.NewPortofolio} />
        <NewContact data={this.state.landingPageData.NewContact} />
      </div>
    )
  }
}

export default App;
