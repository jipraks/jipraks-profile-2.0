import React, { Component } from "react";

export class NewPortofolio extends Component {
  render() {
    return (
      <div id="_portfolio" className="text-center">
        <div className="container" style={{ marginBottom: 50, marginTop: 35 }}>
          <div className="section-title">
            <h2>Portofolio</h2>
          </div>

          <div className="row">
            <div className="portfolio-items">

              {this.props.data ? this.props.data.map((d, i) => (

                <div key={d.projectTitle} className="col-sm-6 col-md-4 col-lg-4">
                  <div className="portfolio-item">
                    <div className="hover-bg">
                      <a href="#_portofolio" title={d.projectTitle} >
                        <div className="hover-text">
                          <h4>{d.projectTitle}</h4>
                          <p style={{ fontStyle: 'italic' }}>{d.role} at {d.company}</p>
                          <p style={{ paddingLeft: 10, paddingRight: 10 }}>{d.description}</p>
                          <p style={{ marginTop: 5 }}>
                          {d.icons.map((i) => (
                            <i style={{ paddingLeft: 5, paddingRight: 5 }} className={i}></i>
                          ))}
                          </p>
                        </div>
                        <img src={d.image} className="img-responsive" alt="Project Title" />
                      </a>
                    </div>
                  </div>
                </div>

              )) : "loading..."}

            </div>
          </div>

          <div style={{ marginTop: 30 }} className="row">
            <h4 style={{ fontStyle: 'italic' }}>And many others projects that I can't publish here because it's confidentiality.</h4>
          </div>

        </div>
      </div>
    );
  }
}

export default NewPortofolio;
