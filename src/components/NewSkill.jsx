import React, { Component } from 'react'

export class NewSkill extends Component {
  render() {
    return (
      <div id="_skill">
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-md-6"> <img src="img/about.jpg" className="img-responsive" alt="" /> </div>
            <div className="col-xs-12 col-md-6">
              <div className="_skill-text">
                <h2>Skill</h2>
                <p>{this.props.data ? this.props.data.paragraph : 'loading...'}</p>

                <div className="list-style">
                  <div className="col-lg-7 col-sm-7 col-xs-12">
                    <h3>Tech Stack I Used Before</h3>
                    <ul>
                      {this.props.data ? this.props.data.Why2.map((d, i) => <li key={`${d}-${i}`}>{d}</li>) : 'loading'}
                    </ul>
                  </div>
                  <div className="col-lg-5 col-sm-5 col-xs-12">
                    <h3>What can I do?</h3>
                    <ul>
                      {this.props.data ? this.props.data.Why.map((d, i) => <li key={`${d}-${i}`}>{d}</li>) : 'loading'}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default NewSkill
